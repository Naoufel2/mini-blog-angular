import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../core/service/auth.service';
import { User } from '../../core/model/user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RoleName } from 'src/app/core/model/enum-role-name';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    form: FormGroup;
    isSignUpFailed = false;
    errorMessage = '';
    userRegister: User;

    constructor(private authService: AuthService,
        private fb: FormBuilder,
        private router: Router) { }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        this.form = this.fb.group({
            firstName: ['', Validators.minLength(3)],
            lastName: ['',  Validators.minLength(3)],
            email: ['', Validators.email],
            password: ['', Validators.required]
        });
    }

    onSubmit() {

        this.userRegister = new User();

        this.userRegister = this.form.value;
        let role: string[] = [RoleName.ROLE_ADMIN];

        this.authService.signUp(this.userRegister, role).subscribe(
            data => {
                this.isSignUpFailed = false;
                this.router.navigate(['/login'], {state: {message:"User registered successfully! login"}})
            },
            error => {
                this.errorMessage = error.error.message;
                this.isSignUpFailed = true;
            }
        );
    }

}
