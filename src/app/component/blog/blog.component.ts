import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { ApiService } from '../../core/service/api.service'
import { StatusInfo, Status } from 'src/app/core/model/status.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserInfo } from 'src/app/core/model/user.model';
import { AuthService } from 'src/app/core/service/auth.service';
import { Comment } from 'src/app/core/model/comment.model';
import { PopupComponent } from 'src/app/layout/popup/popup.component';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

    listStatus: StatusInfo[];
    status: Status;
    comment: Comment;
    userInfo: UserInfo;
    successMessage: string = null;
    errorMessage: string = null;

    formStatus: FormGroup;
    formComment: FormGroup;

    constructor(private apiService: ApiService,
        private authService: AuthService,
        private fb: FormBuilder,
        private dialog: MatDialog) {}

    ngOnInit() {
        this.getAllStatus();
        this.initForm();
        this.getUser();
    }

    getUser() {
        this.userInfo = this.authService.currentUserValue;
    }

    initForm() {
        this.formStatus = this.fb.group({
            textStatus: [''],
        });

        this.formComment = this.fb.group({
            textComment: [''],
        });
    }

    getAllStatus() {
        this.apiService.getAllStatus().subscribe((listStatus: StatusInfo[]) =>{
            this.listStatus = listStatus;
        });
    }

    onSubmitStatus() {
        this.status = new Status(
            this.formStatus.get('textStatus').value,
            new Date(),
            this.userInfo.id
        )
        this.apiService.createStatus(this.status).subscribe(data => {
            this.successMessage = data.message;
            this.getAllStatus();
            this.formStatus.reset();
        },
        error => {
            this.errorMessage = error.error.message;
        })
    }

    onSubmitComment(status_id) {
        this.comment = new Comment(
            this.formComment.get('textComment').value,
            new Date(),
            this.userInfo.id,
            status_id
        )
        this.apiService.createComment(this.comment).subscribe(data => {
            this.successMessage = data.message;
            this.getAllStatus();
            this.formComment.reset();
        },
        error => {
            this.errorMessage = error.error.message;
        })
    }

    deleteSatus(id: number) {
        this.apiService.deleteStatus(id).subscribe(data => {
            this.successMessage = data.message;
            this.getAllStatus();
        },
        error => {
            this.errorMessage = error.error.message;
        })
    }

    deleteComment(id: number) {
        this.apiService.deleteComment(id).subscribe(data => {
            this.successMessage = data.message;
            this.getAllStatus();
        },
        error => {
            this.errorMessage = error.error.message;
        })
    }

    openPopupStatus(id: number): void {
        const dialogRef = this.dialog.open(PopupComponent, {
          width: '340px',
          data: ' Status'
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if(result) {
            this.deleteSatus(id);
          }
        });
    }

    openPopupComment(id: number): void {
        const dialogRef = this.dialog.open(PopupComponent, {
          width: '340px',
          data: 'Comment'
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if(result) {
            this.deleteComment(id);
          }
        });
    }

}