import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../core/service/auth.service';
import { UserLogin } from '../../core/model/user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  isLoginFailed = false;
  loginMessage: string = null;
  private userLogin: UserLogin;

  constructor(private authService: AuthService,
    private router: Router,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.initForm();
    if(window.history.state && window.history.state.message != null) {
      this.loginMessage =  window.history.state.message;
    }
  }

  initForm() {
    this.form = this.fb.group({
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    this.userLogin = new UserLogin(
      this.form.get('email').value,
      this.form.get('password').value);

    this.authService.signIn(this.userLogin).subscribe(
      data => {
        this.router.navigate([''])
      },
      error => {
        this.isLoginFailed = true;
      }
    );
  }
}
