import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './component/login/login.component'
import { RegisterComponent } from './component/register/register.component'
import { BlogComponent } from './component/blog/blog.component';
import { AuthGuardService } from './core/service/auth-guard.service';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'blog', component: BlogComponent, canActivate: [ AuthGuardService ]},
  { path: '', component: BlogComponent, canActivate: [ AuthGuardService ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
