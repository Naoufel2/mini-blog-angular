import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/service/auth.service';
import { Router } from '@angular/router';
import { UserInfo } from 'src/app/core/model/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    userInfo: UserInfo;

    constructor(public authService:AuthService, private router: Router ){ }
    
    ngOnInit() {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.getConnectedUser();
    }

    getConnectedUser(){
        this.authService.currentUser.subscribe((user: UserInfo) => {
            this.userInfo = user;
        });
    }

    logout() {
        this.authService.logout();
        this.router.navigate(['login']);
    }

}
