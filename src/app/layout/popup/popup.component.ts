import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
    selector: 'app-popup',
    templateUrl: 'popup.component.html',
  })
  export class PopupComponent {
  
    constructor(
      public dialogRef: MatDialogRef<PopupComponent>,
      @Inject(MAT_DIALOG_DATA) public message: string) {}
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  
}