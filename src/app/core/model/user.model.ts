import { Role } from "./role";

export class User {

    public firstName: string;
    public lastName: string;
    public email: string;
    public password: string;

    constructor() {};

}

export class UserInfo {

    public id: number;
    public firstName: string;
    public lastName: string;
    public email: string;
    public roles: Role[];
    
}

export class UserLogin {
    email: string;
    password: string;

    constructor(email: string, password: string) {
        this.email = email;
        this.password = password;
    }
}
