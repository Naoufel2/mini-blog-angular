import { RoleName } from "./enum-role-name";

export class Role {
    public id: number;
    public name: RoleName
}