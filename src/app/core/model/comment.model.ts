import { UserInfo } from './user.model';

export class Comment {

    public textComment: string;
    public createdAt: Date;
    public user_id: number;
    public status_id: number;

    constructor(textComment: string, createdAt: Date, user_id: number, status_id: number) {
        this.textComment = textComment;
        this.createdAt = createdAt;
        this.user_id = user_id;
        this.status_id = status_id;
    }

}

export class CommentInfo {

    public id: number;
    public textComment: string;
    public createdAt: Date;
    public user: UserInfo;

}