
export enum  RoleName {
    ROLE_USER = 'user',
    ROLE_PM = 'pm',
    ROLE_ADMIN = 'admin'
}