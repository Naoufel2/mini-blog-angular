import { CommentInfo } from './comment.model';
import { UserInfo } from './user.model';

export class Status {

    public textStatus: string;
    public createdAt: Date;
    public user_id: number;

    constructor(textStatus: string, createdAt: Date, user_id: number) {
        this.textStatus = textStatus;
        this.createdAt = createdAt;
        this.user_id = user_id;
    }

}

export class StatusInfo {

    public id: number;
    public textStatus: string;
    public createdAt: Date;
    public comments: CommentInfo[];
    public user: UserInfo;

}