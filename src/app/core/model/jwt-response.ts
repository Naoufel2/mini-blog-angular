import { UserInfo } from './user.model';

export class JwtResponse {
    accessToken: string;
    type: string;
    userInfo: UserInfo;
}
