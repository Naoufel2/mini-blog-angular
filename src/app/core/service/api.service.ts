import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { StatusInfo, Status } from '../model/status.model';
import { Comment } from '../model/comment.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {


  constructor(private http: HttpClient) {
  }

  getAllStatus():  Observable<StatusInfo[]>  {
    return this.http.get<StatusInfo[]>(environment.apiUrl +'/status');
  }

  createStatus(status: Status): Observable<any> {
    return this.http.post<any>(environment.apiUrl +'/status', status);
  }

  createComment(comment: Comment): Observable<any> {
    return this.http.post<any>(environment.apiUrl +'/comment', comment);
  }

  deleteStatus(id: number): Observable<any> {
    return this.http.delete<any>(environment.apiUrl +'/status/'+id);
  }

  deleteComment(id: number): Observable<any> {
    return this.http.delete<any>(environment.apiUrl +'/comment/'+id);
  }
   
}
