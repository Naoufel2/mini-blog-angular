import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from "rxjs/operators";

import { TokenStorageService } from './token-storage.service';
import { JwtResponse } from '../model/jwt-response';
import { UserLogin, User, UserInfo } from '../model/user.model'
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<UserInfo>;
  public currentUser: Observable<UserInfo>;

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) {
      this.currentUserSubject = new BehaviorSubject<UserInfo>(JSON.parse(this.tokenStorage.getUser()));
      this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): UserInfo {
      return this.currentUserSubject.value;
  }

  signIn(credentials: UserLogin): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(environment.authApiUrl +'/signin', credentials).pipe(
      map(userData => {
        this.tokenStorage.saveToken(userData.accessToken);
        this.tokenStorage.saveUser(userData.userInfo);
        this.currentUserSubject.next(userData.userInfo);
        return userData;
      })
    );
  }

  signUp(userRegister: User, role: string[]): Observable<string> {    
    return this.http.post<string>(environment.authApiUrl +'/signup?role='+role, userRegister);
  }


  isAuthenticated(): boolean {
    if(this.currentUserValue != null) {
      return true;
    }
    return false;
  }

  logout() {
    // remove user from local storage to log user out
    this.tokenStorage.clearStorage();
    this.currentUserSubject.next(null);
  }
}
