import { Injectable } from '@angular/core';
import { UserInfo } from '../model/user.model';

const TOKEN_KEY = 'AuthToken';
const USER = 'AuthUsername';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  constructor() { }

  signOut() {
    localStorage.clear();
  }

  public saveToken(token: string) {
    localStorage.removeItem(TOKEN_KEY);
    localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
  }


  public saveUser(user: UserInfo) {
    localStorage.removeItem(USER);
    localStorage.setItem(USER, JSON.stringify(user));
  }

  public getUser(): string {
    return localStorage.getItem(USER);
  }

  public clearStorage() {
    localStorage.clear();
  }

}
